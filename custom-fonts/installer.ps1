$sourceFile = "IBM_Custom_Fonts_OFL.zip"
$fontsDestinationPath = "$env:SystemRoot\Fonts"  # Windows font directory

# Create the Shell.Application COM object
$objShell = New-Object -ComObject Shell.Application

# Unzip the file
Expand-Archive -Path $sourceFile

$decompressedFolder = $sourceFile -replace '\.[^.]+$'

Write-Host $decompressedFolder

# Get all subfolders containing font files
$subfolders = Get-ChildItem -Path $decompressedFolder -Directory

foreach ($subfolder in $subfolders) {
    $fontFiles = Get-ChildItem -Path $subfolder.FullName -Filter "*.ttf" -File
    foreach ($fontFile in $fontFiles) {
        # Copying into Windows/Fonts doesn't work, gotta do this weird stuff
        $CopyOptions = 4 + 16;
        $objFolder = $objShell.Namespace(0x14)  # Fonts folder
        $dest = Join-Path $fontsDestinationPath $fontFile.Name
        if (Test-Path -Path $dest) {
            Write-Host "Font $($fontFile.Name) already installed"
        } else {
            Write-Host "Installing $($fontFile.Name)"
            $CopyFlag = [String]::Format("{0:x}", $CopyOptions);
            $objFolder.CopyHere($fontFile.FullName, $CopyFlag)
        }
    }
}

# Clean up: Remove the decompressed folder
Remove-Item -Path $decompressedFolder -Recurse -Force
