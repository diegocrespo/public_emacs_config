#!/bin/bash
# Check if unzip is available
if ! command -v unzip &>/dev/null; then
    echo "Error: You need 'unzip' installed to unzip the font file."
    exit 1
fi
# Set the path to the font zip file
font_zip="IBM_Custom_Fonts_OFL.zip"

# Set the target font directory based on the operating system
if [[ "$OSTYPE" == "darwin"* ]]; then
    target_dir="/Library/Fonts"  # macOS font directory
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    target_dir="/usr/share/fonts"  # Linux font directory
else
    echo "Unsupported operating system."
    exit 1
fi

# Unzip the font file
unzip "$font_zip" -d fonts_temp

# Copy fonts from each folder to the target directory
find fonts_temp -type f -iname "*.ttf" -exec cp {} "$target_dir" \;

# Update the font cache based on the operating system
if [[ "$OSTYPE" == "darwin"* ]]; then
    sudo atsutil databases -remove  # Clear macOS font cache
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    sudo fc-cache -f -v  # Update Linux font cache
fi

# Remove the temporary font directory
rm -rf fonts_temp
