<#
.SYNOPSIS
This script downloads and installs the Zig programming language to compile tree-sitter grammars.

.DESCRIPTION
The script handles the installation of Zig and the compilation of various tree-sitter grammars for language parsing. 
It supports platform-agnostic operations for Windows, macOS, and Linux. 
Features include checking for existing installations, supporting reinstallation, and downloading and extracting archives in a platform-appropriate manner.

.PARAMETER ReinstallZig
Specifies whether to reinstall Zig if it is already installed.

.PARAMETER ReinstallGrammar
Indicates whether to reinstall tree-sitter grammars if they are already present.

.EXAMPLE
.\install-ts-grammars.ps1 -ReinstallZig
This command reinstalls Zig, downloading the latest version specified in the script.

.EXAMPLE
.\install-ts-grammars.ps1 -ReinstallGrammar
This command reinstalls all specified tree-sitter grammars.

.NOTES
The script uses conditional logic to handle different operating systems and architectures, 
leveraging native commands and utilities where possible.
#>

param (
    [switch]$ReinstallZig,
    [switch]$ReinstallGrammar
)

function Get-TempDirectory {
    # Platform agnostic way of getting temp dir
    if ($IsWindows) {
        return [System.IO.Path]::GetTempPath()
    } else {
        return "/tmp"
    }
}

function Download-And-Extract-Zig {
    #Given a URL and a temporary directory, downloads the Zig archive and extracts it to the script's root directory.
    param(
        [string]$url,
        [string]$tempDir
    )
    
    $fileName = [System.IO.Path]::GetFileName($url)
    $tempPath = Join-Path $tempDir $fileName
    Write-Host "Installing $url`n"
    Invoke-WebRequest -Uri $url -OutFile $tempPath

    if ($IsWindows) {
        $extractPath = Join-Path $tempDir (New-Guid).Guid
        Expand-Archive -LiteralPath $tempPath -DestinationPath $extractPath
        Get-ChildItem $extractPath -Recurse | Move-Item -Destination $PSScriptRoot -Force
        Remove-Item $extractPath -Recurse
    } else {
        tar -xf $tempPath -C $PSScriptRoot
    }

    Write-Host "Cleaning up $tempPath"
    Remove-Item $tempPath
}

$ZigVersion = "0.11.0"

$fileExt = if ($IsWindows) { ".dll" } elseif ($IsLinux) { ".so" } elseif ($IsMacOS) { ".dylib" } else { throw "Unsupported OS" }

# Get the temp directory
$tempDir = Get-TempDirectory

# Determine OS and Architecture
$osArch = if ($IsWindows) { "windows-x86_64" } elseif ($IsMacOS) {
    if ([System.Runtime.InteropServices.RuntimeInformation]::OSArchitecture -eq "Arm64") {
        "macos-aarch64"
    } else {
        "macos-x86_64"
    }
} elseif ($IsLinux) { "linux-x86_64" } else { "unsupported" }

# I.E zig-windows-x86_64-0.11.0
$zigOsArch = "zig-$osArch-$ZigVersion"

# Construct the download URL with the Zig version variable
$downloadZigUrl = switch ($osArch) {
    "windows-x86_64" { "https://ziglang.org/download/$ZigVersion/$zigOsArch.zip" }
    "macos-aarch64" { "https://ziglang.org/download/$ZigVersion/$zigOsArch.tar.xz" }
    "macos-x86_64" { "https://ziglang.org/download/$ZigVersion/$zigOsArch.tar.xz" }
    "linux-x86_64" { "https://ziglang.org/download/$ZigVersion/$zigOsArch.tar.xz" }
    Default { throw "Unsupported platform: $osArch" }
}

# Check if Zig directory already exists and -ReinstallZig is not used
if ((Test-Path -Path "./$zigOsArch") -and (-not $ReinstallZig)) {
    Write-Host "$zigOsArch already downloaded. Skipping install. Pass -ReinstallZig to redownload."
}
elseif ($ReinstallZig -And (Test-Path -Path "./$zigOsArch")) {

    Write-Host "Removing old Zig version $zigOsArch"
    Remove-Item -Path "./$zigOsArch" -Recurse -Force
    # Get the temp directory
    $tempDir = Get-TempDirectory

    # Download and extract the file
    Download-And-Extract-Zig -url $downloadZigUrl -tempDir $tempDir
    Write-Host "Platform specific Zig has been redownloaded and extracted to: $PSScriptRoot"
}
else {

# Download and extract the file
Download-And-Extract-Zig -url $downloadZigUrl -tempDir $tempDir

Write-Host "Zig has been downloaded and extracted to: $PSScriptRoot"
}


##### Install Tree-sitter grammars #####
$grammarUrls = @(
    "https://github.com/camdencheek/tree-sitter-dockerfile",
    "https://github.com/tree-sitter/tree-sitter-bash",
    "https://github.com/uyha/tree-sitter-cmake",
    "https://github.com/tree-sitter/tree-sitter-css",
    "https://github.com/tree-sitter/tree-sitter-c-sharp",
    "https://github.com/tree-sitter/tree-sitter-javascript",
    "https://github.com/tree-sitter/tree-sitter-json",
    "https://github.com/tree-sitter/tree-sitter-python",
    "https://github.com/ikatyang/tree-sitter-yaml"
)
$zigCompiler = (Get-ChildItem zig-*/zig).FullName

foreach ($url in $grammarUrls) {
    # I.E tree-sitter-markdown 
    $repoName = $url -split '/' | Select-Object -Last 1
    # I.E tree-sitter-markdown to markdown
    $languageName = $repoName -replace 'tree-sitter-', ''
    $cloneDir = Join-Path $tempDir $repoName

    # Check if grammar directory already exists and -ReinstallGrammar is not used
    if ((Test-Path -Path $cloneDir) -and (-not $ReinstallGrammar)) {
        Write-Host "$repoName already cloned. Pass -ReinstallGrammar to reclone."
        continue
    }

    # If -ReinstallGrammar is used, clean up the current grammar directory
    if ($ReinstallGrammar -And (Test-Path -Path $cloneDir)) {
        Write-Host "Removing old version of $repoName"
        Remove-Item -Path $cloneDir -Recurse -Force
    }

    Write-Host "Downloaded $url to $cloneDir"
    # Clone the grammar repository
    git clone $url $cloneDir --depth=1

    # Navigate to the cloned directory
    Push-Location -Path $cloneDir

    # Compile the grammar using zig cc
    $libName = "libtree-sitter-$languageName$fileExt"
    $cSrcFiles = (Get-ChildItem src/*.c).FullName
    # Hack to filter schema.generated.cc from the yaml ts grammar
    # It's already included in scanner.cc
    $cppSrcFiles = Get-ChildItem src/*.cc
    | Where-Object { $_.Name -ne "schema.generated.cc" } | ForEach-Object { $_.FullName }
    $includeDirs = @('-Isrc -Isrc/tree-sitter')
    # Check if there are C++ source files
    if ($cppSrcFiles -ne $null) {
	# Compile C++ files using zig c++
	[System.Collections.ArrayList]$cppObjectFiles = @()
	foreach ($fileName in $cppSrcFiles) {
	    $cppObject = $fileName + ".o"
	    $cppObjectFiles.Add($cppObject) | Out-Null
	    $cppCmd = "$zigCompiler c++ -c -fPIC $fileName -o $cppObject $($includeDirs -join ' ' ) -lc"
	    Write-Host "Compiling C++ files with the command: $cppCmd`n"
	    Invoke-Expression $cppCmd
	}

	[System.Collections.ArrayList]$cObjectFiles = @()
	foreach ($fileName in $cSrcFiles) {
	    $cObject = $fileName + ".o"
	    $cObjectFiles.Add($cObject) | Out-Null
	    $cCmd = "$zigCompiler cc -c -fPIC $fileName -o $cObject $($includeDirs -join ' ')"
	    Write-Host "Compiling C files with the command: $cCmd`n"
	    Invoke-Expression $cCmd
	}

	$zigCmd = "$zigCompiler c++ -shared -o $libName $($cppObjectFiles -join ' ') $($cOjbectFiles -join ' ')"
	Write-Host "Compiling Shared files with the command: $zigCmd`n"
	Invoke-Expression $zigCmd
    }
    else {
	Write-Host "No .cc files detected just compiling C files`n"
    # Construct the zig cc command for compiling C files
    $zigCmd = "$zigCompiler cc -shared -o $libName $cSrcFiles $($includeDirs -join ' ')"
    Write-Host "Compiling C files with the command: $zigCmd"
    Invoke-Expression $zigCmd
    }


    # Move the compiled file to the tree-sitter directory in the current script's directory
    $scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path

    $destinationDir = Join-Path $scriptDir "tree-sitter"
    if (-not (Test-Path -Path $destinationDir)) {
        Write-Host "The 'tree-sitter' directory does not exist in the current script directory. Creating it"
	New-Item -Path $destinationDir -ItemType Directory
	Write-Host "Created '$destinationDir' directory."
    } else {
	# This will fail if Emacs is open and using the tree-sitter grammars
        Move-Item -Path $libName -Destination $destinationDir -Force
    }

    # Move back to the original directory
    Pop-Location
}

Write-Host "All grammars have been cloned and compiled."
