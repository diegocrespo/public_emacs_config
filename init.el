;;;;; Emacs specific configurations ;;;;;
;; By Diego Crespo
;; In use since January 20th, 2017

;; Set the directory for backup files
;; Function to ensure a directory exists
(defun ensure-directory-exists (dir)
  (unless (file-directory-p dir)
    (make-directory dir t)))
	
(defun build-project ()
  (interactive)
  (let* ((ps1-file-exists (file-exists-p "build.ps1"))
         (sh-file-exists (file-exists-p "build.sh")))
    (if (or ps1-file-exists sh-file-exists)
        (let* ((script-name (if ps1-file-exists "pwsh ./build.ps1" "./build.sh"))
               (output-buffer-name "*build-output*")
               (shell-output (with-output-to-string
                               (with-current-buffer standard-output
                                 (shell-command script-name t nil)))))
          ;; Check if there is any previous buffer with the same name and kill it
          (when (get-buffer output-buffer-name)
            (kill-buffer output-buffer-name))
          ;; Check if shell output is non-empty and contains more than just whitespace
          (when (and shell-output (string-match-p "\\S+" shell-output))
            ;; Create or get the buffer, insert the shell output, and display it
            (with-current-buffer (get-buffer-create output-buffer-name)
              (insert shell-output)
              (display-buffer (current-buffer)))))
      ;; If neither file exists, call M-x compile
      (call-interactively 'compile))))

(defun open-file-in-new-tab (filename)
  "Open a file in a new tab."
  (interactive "FOpen file in new tab: ")
  (tab-new)
  (find-file filename))

(defun open-terminal-in-new-tab ()
  "Open a new tab and start an ANSI terminal in it."
  (interactive)
  (tab-new) 
  (ansi-term (getenv "SHELL")) ; ANSI terminal with the default shell
)

(ensure-directory-exists "~/.emacs.d/backups")
(ensure-directory-exists "~/.emacs.d/autosaves")
(ensure-directory-exists "~/.emacs.d/offline-packages")
(ensure-directory-exists "~/.emacs.d/snippets")
(ensure-directory-exists "~/.emacs.d/themes")
(ensure-directory-exists "~/.emacs.d/custom-fonts")
(ensure-directory-exists "~/.emacs.d/")

;; Directories for backups and autosaves
(setq my-backup-directory "~/.emacs.d/backups/")
(setq my-autosave-directory "~/.emacs.d/autosaves/")

;; Set the directories for backup and autosave files
(setq backup-directory-alist
      `((".*" . ,my-backup-directory)))

(setq auto-save-file-name-transforms
      `((".*" ,my-autosave-directory t)))

; when running emacs in gui mode
(when window-system
    (tool-bar-mode -1)
    (tooltip-mode -1)
    (scroll-bar-mode -1)
    (menu-bar-mode -1))

(setq visible-bell 1)
(electric-pair-mode 1)

;; add time to modeline
(display-time)
(setq display-time-format "%H:%M") 
(setq inhibit-startup-screen t) ; inhibit the start up screen
(setq initial-scratch-message "")
(setq tramp-default-method "ssh") ; So you don't have to type ssh:user@url

;; I hate accidentally closing emacs. So make that impossible
(global-set-key (kbd "C-x C-c") 'suspend-frame)
(global-display-line-numbers-mode 1)

(setq display-line-numbers-type 'relative)

;;;;; themes ;;;;;
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;;(load-theme 'dracula t)
(load-theme 'monokai t)

(set-face-attribute 'default nil :height 140)

;;;;; offline packages ;;;;;

(use-package evil
  :load-path "~/.emacs.d/offline-packages/evil" ; Specify where to load the package from
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(tab-bar-mode 1)

(use-package evil-leader
  :load-path "~/.emacs.d/offline-packages/evil-leader"
  :requires evil ; Ensure 'evil' package is loaded before 'evil-leader'
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "f" 'switch-to-buffer
    "i i" 'bookmark-bmenu-list
    "i o" 'bookmark-jump
    "i p" 'bookmark-set
    "i e" 'find-file
    "t o" 'open-file-in-new-tab
    "t x" 'tab-bar-close-tab
    "t n" 'tab-bar-switch-to-next-tab
    "t p" 'tab-bar-switch-to-prev-tab
    "t d" 'tab-duplicate
    "t t" 'open-terminal-in-new-tab
    "g f" 'forward-sexp
    "g b" 'backward-sexp
    "g k" 'kill-sexp
    "g m" 'mark-sexp
    "i d" 'list-buffers
    ";" 'comment-or-uncomment-region
    "u" 'kill-buffer
    "k r" 'query-replace-regexp
    ", e" 'eval-buffer
    "l d" 'eshell
    "l <SPC>" 'whitespace-mode
    "l g" 'make-frame
    "k ," 'other-frame
    "k o" 'other-window
    "e" 'delete-other-windows
    "k y" 'goto-line
    "n" 'flymake-goto-next-error
    "p" 'flymake-goto-prev-error
    "t r" 'eglot-rename
    "x f" 'eglot-find-declaration
    "x o" 'xref-goto-xref
    "c c" 'build-project
    "c f" 'eglot-format-buffer)
;; This must come after the set keys to work properly
(global-evil-leader-mode))

;; Required before evil-collection can be loaded
(add-to-list 'load-path "~/.emacs.d/offline-packages/annalist")
(use-package annalist
  :load-path "~/.emacs.d/offline-packages/annalist")

(use-package evil-collection
  :load-path "~/.emacs.d/offline-packages/evil-collection"
  :custom (evil-collection-setup-minibuffer t)
  :after evil
  :config
  (evil-collection-init))

;; (use-package python-mode
;;   :hook (python-mode . eglot-ensure)
;;   :config
;;   ;; Assuming eglot is already installed or declared elsewhere
;;   ;; This sets up eglot for python-mode specifically
;;   (add-to-list 'eglot-server-programs '(python-mode . ("pylsp")))
;;   (setq eglot-workspace-configuration
;;         '((:pylsp . ((:configurationSources . ["pycodestyle"])
;;                      (:plugins . ((:autopep8 . ((:enabled . t)))
;;                                   (:flake8 . ((:config . nil)
;;                                               (:enabled . :json-false)
;;                                               (:exclude . [])
;;                                               (:executable . "flake8")
;;                                               (:extendIgnore . [])
;;                                               (:filename . nil)
;;                                               (:hangClosing . nil)
;;                                               (:ignore . [])
;;                                               (:indentSize . nil)
;;                                               (:maxComplexity . nil)
;;                                               (:maxLineLength . nil)
;;                                               (:perFileIgnores . [])
;;                                               (:select . nil)))
;;                                   (:jedi . ((:auto_import_modules . ["numpy"])
;;                                             (:env_vars . nil)
;;                                             (:environment . nil)
;;                                             (:extra_paths . [])))
;;                                   (:jedi_completion . ((:cache_for . ["pandas" "numpy" "tensorflow" "matplotlib"])
;;                                                        (:eager . :json-false)
;;                                                        (:enabled . t)
;;                                                        (:fuzzy . :json-false)
;;                                                        (:include_class_objects . :json-false)
;;                                                        (:include_function_objects . :json-false)
;;                                                        (:include_params . t)
;;                                                        (:resolve_at_most . 25)))
;;                                   (:jedi_definition . ((:enabled . t)
;;                                                        (:follow_builtin_definitions . t)
;;                                                        (:follow_builtin_imports . t)
;;                                                        (:follow_imports . t)))
;;                                   (:jedit_hover . ((:enabled . t)))
;;                                   (:jedi_references . ((:enabled . t)))
;;                                   (:jedi_signature_help . ((:enabled . t)))
;;                                   (:jedi_symbols . ((:all_scopes . t)
;;                                                     (:enabled . t)
;;                                                     (:include_import_symbols . t)))
;;                                   (:mccabe . ((:enabled . t)
;;                                               (:threshold . 15)))
;;                                   (:preload . ((:enabled . t)
;;                                                (:modules . [])))
;;                                   (:pycodestyle . ((:enabled . t)
;;                                                    (:exclude . [])
;;                                                    (:filename . [])
;;                                                    (:hangClosing . nil)
;;                                                    (:ignore . [])
;;                                                    (:indentSize . nil)
;;                                                    (:maxLineLength . nil)
;;                                                    (:select . nil)))
;;                                   (:pydocstyle . ((:addIgnore . [])
;;                                                   (:addSelect . [])
;;                                                   (:convention . nil)
;;                                                   (:enabled . :json-false)
;;                                                   (:ignore . [])
;;                                                   (:match . "(?!test_).*\\.py")
;;                                                   (:matchDir . "[^\\.].*")
;;                                                   (:select . nil)))
;;                                   (:pyflakes . ((:enabled . t)))
;;                                   (:pylint . ((:args . [])
;;                                               (:enabled . :json-false)
;;                                               (:executable . nil)))
;;                                   (:rope_autoimport . ((:code_actions . ((:enabled . t)))
;;                                                        (:completions . ((:enabled . t)))
;;                                                        (:enabled . :json-false)
;;                                                        (:memory . :json-false)))
;;                                   (:rope_completion . ((:eager . :json-false)
;;                                                        (:enabled . :json-false)))
;;                                   (:yapf . ((:enabled . t)))))
;;                      (:rope . ((:extensionModules . nil)
;;                                (:ropeFolder . nil))))))))


(use-package python-ts-mode
  :hook (python-ts-mode . eglot-ensure)
  :config
  ;; Assuming eglot is already installed or declared elsewhere
  ;; This sets up eglot for python-mode specifically
  (add-to-list 'eglot-server-programs '(python-ts-mode . ("pylsp")))
  (add-hook 'python-ts-mode-hook
	    (lambda ()
	      (setq-local python-shell-interpreter (pet-executable-find "python")
			  python-shell-virtualenv-root (pet-virtualenv-root))))
  (setq eglot-workspace-configuration
        '((:pylsp . ((:configurationSources . ["pycodestyle"])
                     (:plugins . ((:autopep8 . ((:enabled . t)))
                                  (:flake8 . ((:config . nil)
                                              (:enabled . :json-false)
                                              (:exclude . [])
                                              (:executable . "flake8")
                                              (:extendIgnore . [])
                                              (:filename . nil)
                                              (:hangClosing . nil)
                                              (:ignore . [])
                                              (:indentSize . nil)
                                              (:maxComplexity . nil)
                                              (:maxLineLength . nil)
                                              (:perFileIgnores . [])
                                              (:select . nil)))
                                  (:jedi . ((:auto_import_modules . ["numpy"])
                                            (:env_vars . nil)
                                            (:environment . nil)
                                            (:extra_paths . [])))
                                  (:jedi_completion . ((:cache_for . ["pandas" "numpy" "tensorflow" "matplotlib"])
                                                       (:eager . :json-false)
                                                       (:enabled . t)
                                                       (:fuzzy . :json-false)
                                                       (:include_class_objects . :json-false)
                                                       (:include_function_objects . :json-false)
                                                       (:include_params . t)
                                                       (:resolve_at_most . 25)))
                                  (:jedi_definition . ((:enabled . t)
                                                       (:follow_builtin_definitions . t)
                                                       (:follow_builtin_imports . t)
                                                       (:follow_imports . t)))
                                  (:jedit_hover . ((:enabled . t)))
                                  (:jedi_references . ((:enabled . t)))
                                  (:jedi_signature_help . ((:enabled . t)))
                                  (:jedi_symbols . ((:all_scopes . t)
                                                    (:enabled . t)
                                                    (:include_import_symbols . t)))
                                  (:mccabe . ((:enabled . t)
                                              (:threshold . 15)))
                                  (:preload . ((:enabled . t)
                                               (:modules . [])))
                                  (:pycodestyle . ((:enabled . t)
                                                   (:exclude . [])
                                                   (:filename . [])
                                                   (:hangClosing . nil)
                                                   (:ignore . [])
                                                   (:indentSize . nil)
                                                   (:maxLineLength . nil)
                                                   (:select . nil)))
                                  (:pydocstyle . ((:addIgnore . [])
                                                  (:addSelect . [])
                                                  (:convention . nil)
                                                  (:enabled . :json-false)
                                                  (:ignore . [])
                                                  (:match . "(?!test_).*\\.py")
                                                  (:matchDir . "[^\\.].*")
                                                  (:select . nil)))
                                  (:pyflakes . ((:enabled . t)))
                                  (:pylint . ((:args . [])
                                              (:enabled . :json-false)
                                              (:executable . nil)))
                                  (:rope_autoimport . ((:code_actions . ((:enabled . t)))
                                                       (:completions . ((:enabled . t)))
                                                       (:enabled . :json-false)
                                                       (:memory . :json-false)))
                                  (:rope_completion . ((:eager . :json-false)
                                                       (:enabled . :json-false)))
                                  (:yapf . ((:enabled . t)))))
                     (:rope . ((:extensionModules . nil)
                               (:ropeFolder . nil))))))))
(use-package yasnippet
  :load-path "~/.emacs.d/offline-packages/yasnippet")

(use-package powershell
  :load-path "~/.emacs.d/offline-packages/powershell")

;; string manipulation
(use-package s
  :load-path "~/.emacs.d/offline-packages/s")

;; modern list library for emacs
(use-package dash
  :load-path "~/.emacs.d/offline-packages/dash")

;; api for working with files and directories
(use-package f
  :load-path "~/.emacs.d/offline-packages/f"
  :requires s)

;; python tooling/venv
(use-package pet
  :load-path "~/.emacs.d/offline-packages/emacs-pet"
  :requires (f dash)
  :config
  ;; adding it to the base makes -ts-mode and python mode inherit
  (add-hook 'python-base-mode-hook 'pet-mode -10))

;; ;; api for working with files and directories
;; (use-package compat
;;   :load-path "~/.emacs.d/offline-packages/compat")

;; (use-package transient
;;   :load-path "~/.emacs.d/offline-packages/transient")

;; (use-package with-editor
;;   :load-path "~/.emacs.d/offline-packages/with-editor")

;; (use-package magit
;;   :load-path "~/.emacs.d/offline-packages/magit"
;;   :requires (compat dash transient with-editor))
(cond
 ((eq system-type 'darwin)
  (with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '(python-mode . ("/Users/diego/opt/anaconda3/bin/pylsp" ))))))   

(if (treesit-available-p)
    (progn
      (message "Tree-Sitter is available")
      ;; Shell script files
      (if (treesit-language-available-p 'sh)
          (add-to-list 'auto-mode-alist '("\\.sh\\'" . bash-ts-mode)))

      ;; CSS files
      (if (treesit-language-available-p 'css)
          (add-to-list 'auto-mode-alist '("\\.css\\'" . css-ts-mode)))

      ;; HTML files
      (if (treesit-language-available-p 'html)
          (add-to-list 'auto-mode-alist '("\\.html?\\'" . html-ts-mode))); Matches .html and .htm

      ;; JavaScript and JSON files
      (if (treesit-language-available-p 'javascript)
          (add-to-list 'auto-mode-alist '("\\.js\\'" . js-ts-mode)))

      (if (treesit-language-available-p 'json)
          (add-to-list 'auto-mode-alist '("\\.json\\'" . json-ts-mode)))

      (if (treesit-language-available-p 'cmake)
          (add-to-list 'auto-mode-alist '("CMakeLists\\.txt\\'" . cmake-ts-mode)))


      (if (treesit-language-available-p 'dockerfile)
	  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-ts-mode)))

      ;; Activate yaml-ts-mode for .yaml and .yml files
      (if (treesit-language-available-p 'yaml)
          (add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-ts-mode)))
      (if (treesit-language-available-p 'yaml)
          (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-ts-mode)))

      (if (treesit-language-available-p 'python)
          (add-to-list 'auto-mode-alist '("\\.py\\'" . python-ts-mode)))

      (if (treesit-language-available-p 'c-sharp)
          (add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-ts-mode)))
      ;; Files that start with a #!
      ;; (setq interpreter-mode-alist
      ;;       '(("python" . python-ts-mode)
      ;;         ("sh" . bash-ts-mode)
      ;;         ("bash" . bash-ts-mode)))

      ))


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(evil-collection-master evil-collection evil)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
